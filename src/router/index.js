import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import WhatsAppGroup from '@/components/Portfolio/Portfolio'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Homepage
    },
    {
    	path: '/components/Portfolio/Portfolio',
      component: Portfolio
    }

  ]
})