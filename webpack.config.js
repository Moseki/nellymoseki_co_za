// Option 1
/*module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/sass/base/_all.scss";
          @import "@/sass/components/_all.scss";
          @import "@/sass/fontawesome/_all.scss";
          @import "@/sass/settings/_all.scss";
          @import "@/sass/utils/_mixin_legacy.scss";
          `
      }
    },
    sourcemap: true
  }
};*/

// Option 2
module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/sass/styles.scss";`
      }
    }
  }
};

// Option 3
/*module.exports = {
	css: {
    loaderOptions: {
      sass:{
        data: fs.readFileSync('src/sass/styles.scss', 'utf-8'),
        includePaths: [
          path.resolve(__dirname, 'src/sass')
        ]
      },
    }, 
    sassLoader: {
      sourceMap: true
    }
  }
};*/

// Option 4
/*module: {
  loaders: [
    {
      test: /\.vue$/,
      loader: 'vue'
    }, 
    {
        test: /\.s[a|c]ss$/,
        loader: 'style!css!sass'
    }
  ]
},
vue: {
  loaders: {
    scss: 'style!css!sass'
  }
}*/

// Option 5
/*module.exports = {
  chainWebpack: config => {
    const oneOfsMap = config.module.rule('scss').oneOfs.store
    oneOfsMap.forEach(item => {
      item
        .use('sass-resources-loader')
        .loader('sass-resources-loader')
        .options({
          // Provide path to the file with resources
          resources: './src/sass/styles.scss',
        })
        .end()
    })
  }
}*/

//Option 6
/*module: {
  rules: [
    {
      test: /\.vue$/,
      use: 'vue-loader'
    },
    {
      test: /\.css$/,
      use: [
        { loader: 'vue-style-loader' },
        { loader: 'css-loader', options: { sourceMap: true } },
      ]
    },
    {
      test: /\.scss$/,
      use: [
        { loader: 'vue-style-loader' },
        { loader: 'css-loader', options: { sourceMap: true } },
        { loader: 'sass-loader', options: { sourceMap: true } },
        { loader: 'sass-resources-loader',
          options: {
            sourceMap: true,
            resources: [
              resolveFromRootDir('src/sass/styles.scss'),
            ]
          }
        }
      ]
    }
  ]
}*/


// Option 7
/*const path = require('path');
module.exports = {
  pluginOptions: {
    'css-loader',
    'sass-loader',
    'vue-style-loader',
    'style-resources-loader': {
      'patterns': [
        path.resolve(__dirname, 'src/sass/base/_all.scss'),
        path.resolve(__dirname, 'src/sass/components/_all.scss'),
        path.resolve(__dirname, 'src/sass/fontawesome/_all.scss'),
        path.resolve(__dirname, 'src/sass/settings/_all.scss'),
        path.resolve(__dirname, 'src/sass/utils/_mixin_legacy.scss')
      ]
    }
  }
}*/


// Option 8
/*{
  test: /\.scss$/,
  use: [
    'css-loader',
    'sass-loader',
    'vue-style-loader',
    'style-resources-loader'
    {
      loader: 'sass-loader',
      options: {
        data: `
          @import "@/sass/base/_all.scss";
          @import "@/sass/components/_all.scss";
          @import "@/sass/fontawesome/_all.scss";
          @import "@/sass/settings/_all.scss";
          @import "@/sass/utils/_mixin_legacy.scss";
        `
      }
    }
  ]
}*/